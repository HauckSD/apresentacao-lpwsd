/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teste.lpwsd.bean;

import com.teste.lpwsd.converter.UsuarioConveter;
import com.teste.lpwsd.dao.EmprestimoDao;
import com.teste.lpwsd.dao.ExemplarDao;
import com.teste.lpwsd.dao.LivroDao;
import com.teste.lpwsd.dao.ReservaDao;
import com.teste.lpwsd.dao.UsuarioDao;
import com.teste.lpwsd.dto.EmprestimoDTO;
import com.teste.lpwsd.dto.LivroDTO;
import com.teste.lpwsd.dto.ReservaDTO;
import com.teste.lpwsd.dto.UsuarioDTO;
import com.teste.lpwsd.filter.UsuarioFilter;
import java.io.Serializable;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.enterprise.inject.Produces;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author celio
 */
@ManagedBean(name = "ReservaBean")
@ViewScoped()
public class ReservarBean implements Serializable {
    	private static final long serialVersionUID = 1L;
	
	private LivroDTO livro;
        
	private LivroDao livroDao;

	private List<LivroDTO> livros;
	
	private UsuarioDao usuarioDao;

	private UsuarioDTO usuarioDTO;
        
	private List<UsuarioDTO> usuarios;
        
        private UsuarioFilter usuarioFilter = new UsuarioFilter();
	

	private EmprestimoDao emprestimoDao;

	private List<LivroDTO> emprestimos = new ArrayList<>();

	private List<EmprestimoDTO> emprestimosConsulta = new ArrayList<EmprestimoDTO>();
	

	private ReservaDao reservaDao;

	private List<ReservaDTO> reservas = new ArrayList<>();

        private ExemplarDao exemplarDao = new ExemplarDao();
	private ReservaDTO reservaModel;
	
	private UsuarioDTO usuarioSelecionado = new UsuarioDTO();
	
	private Date dataDevolucao; 

        
        private LivroDTO livroSelecionado;
	

	private ReservaDTO reservaSelecionada;
        
        public ReservarBean() {
           this.Instanciar();
           this.realizarBuscas();
        }
        
        private void Instanciar() { 
            this.setLivro(new LivroDTO());
            this.setLivroSelecionado(new LivroDTO());
            this.setLivroDao(new LivroDao());
            this.setLivros((List<LivroDTO>) new ArrayList());
            this.setUsuarioDao(new UsuarioDao());
            this.setUsuarioDTO(new UsuarioDTO());
            this.setUsuarios((List<UsuarioDTO>) new ArrayList());
            this.setEmprestimoDao(new EmprestimoDao());
            this.setEmprestimos((List<LivroDTO>) new ArrayList());
            this.setEmprestimosConsulta((List<EmprestimoDTO>) new ArrayList());
            this.setReservaDao(new ReservaDao());
            this.setReservas((List<ReservaDTO>) new ArrayList());
            this.setReservaModel(new ReservaDTO());
            this.setReservaSelecionada(new ReservaDTO());
        }
        
        private void realizarBuscas() {
           this.reservas = this.reservaDao.obterReservas();
           this.setLivros(this.exemplarDao.obterExemplares());
           this.setUsuarios(UsuarioConveter.converterList(this.usuarioDao.buscarTodos()));
        }
       

	public void alocarLivroParaReserva(LivroDTO exemplar) {
            this.getEmprestimos().add(exemplar);
            this.setLivro(exemplar);
	}
        
        public void cancelarReserva() {
           this.reservaDao.remover(this.reservaSelecionada.getIdReserva());
           this.realizarBuscas();
        }
	
	public void salvarReservarLivro() {
            this.getLivro().getAno();
            this.usuarioSelecionado = UsuarioConveter.converter(usuarioDao.getUsuario(usuarioFilter));
            this.getReservaDao().salvarReservar(this.getLivro(), this.getUsuarioSelecionado().getIdUsuario(), this.getDataDevolucao());
            this.limparFiltros();
                  
	}
        
        private void limparFiltros() {
            this.setLivroSelecionado(new LivroDTO());
            this.setUsuarioSelecionado(new UsuarioDTO());
            this.setDataDevolucao(null);
        }

    /**
     * @return the livro
     */
    public LivroDTO getLivro() {
        return livro;
    }

    /**
     * @param livro the livro to set
     */
    public void setLivro(LivroDTO livro) {
        this.livro = livro;
    }

    /**
     * @return the livroDao
     */
    public LivroDao getLivroDao() {
        return livroDao;
    }

    /**
     * @param livroDao the livroDao to set
     */
    public void setLivroDao(LivroDao livroDao) {
        this.livroDao = livroDao;
    }

    /**
     * @return the livros
     */
    public List<LivroDTO> getLivros() {
        return livros;
    }

    /**
     * @param livros the livros to set
     */
    public void setLivros(List<LivroDTO> livros) {
        this.livros = livros;
    }

    /**
     * @return the usuarioDao
     */
    public UsuarioDao getUsuarioDao() {
        return usuarioDao;
    }

    /**
     * @param usuarioDao the usuarioDao to set
     */
    public void setUsuarioDao(UsuarioDao usuarioDao) {
        this.usuarioDao = usuarioDao;
    }

    /**
     * @return the usuarioDTO
     */
    public UsuarioDTO getUsuarioDTO() {
        return usuarioDTO;
    }

    /**
     * @param usuarioDTO the usuarioDTO to set
     */
    public void setUsuarioDTO(UsuarioDTO usuarioDTO) {
        this.usuarioDTO = usuarioDTO;
    }

    /**
     * @return the usuarios
     */
    public List<UsuarioDTO> getUsuarios() {
        return usuarios;
    }

    /**
     * @param usuarios the usuarios to set
     */
    public void setUsuarios(List<UsuarioDTO> usuarios) {
        this.usuarios = usuarios;
    }

    /**
     * @return the emprestimoDao
     */
    public EmprestimoDao getEmprestimoDao() {
        return emprestimoDao;
    }

    /**
     * @param emprestimoDao the emprestimoDao to set
     */
    public void setEmprestimoDao(EmprestimoDao emprestimoDao) {
        this.emprestimoDao = emprestimoDao;
    }

    /**
     * @return the emprestimos
     */
    public List<LivroDTO> getEmprestimos() {
        return emprestimos;
    }

    /**
     * @param emprestimos the emprestimos to set
     */
    public void setEmprestimos(List<LivroDTO> emprestimos) {
        this.emprestimos = emprestimos;
    }

    /**
     * @return the emprestimosConsulta
     */
    public List<EmprestimoDTO> getEmprestimosConsulta() {
        return emprestimosConsulta;
    }

    /**
     * @param emprestimosConsulta the emprestimosConsulta to set
     */
    public void setEmprestimosConsulta(List<EmprestimoDTO> emprestimosConsulta) {
        this.emprestimosConsulta = emprestimosConsulta;
    }

    /**
     * @return the reservaDao
     */
    public ReservaDao getReservaDao() {
        return reservaDao;
    }

    /**
     * @param reservaDao the reservaDao to set
     */
    public void setReservaDao(ReservaDao reservaDao) {
        this.reservaDao = reservaDao;
    }

    /**
     * @return the reservas
     */
    public List<ReservaDTO> getReservas() {
        return reservas;
    }

    /**
     * @param reservas the reservas to set
     */
    public void setReservas(List<ReservaDTO> reservas) {
        this.reservas = reservas;
    }

    /**
     * @return the reservaModel
     */
    public ReservaDTO getReservaModel() {
        return reservaModel;
    }

    /**
     * @param reservaModel the reservaModel to set
     */
    public void setReservaModel(ReservaDTO reservaModel) {
        this.reservaModel = reservaModel;
    }



    /**
     * @return the dataDevolucao
     */
    public Date getDataDevolucao() {
        return dataDevolucao;
    }

    /**
     * @param dataDevolucao the dataDevolucao to set
     */
    public void setDataDevolucao(Date dataDevolucao) {
        this.dataDevolucao = dataDevolucao;
    }

    /**
     * @return the livroSelecionado
     */
    public LivroDTO getLivroSelecionado() {
        return livroSelecionado;
    }

    /**
     * @param livroSelecionado the livroSelecionado to set
     */
    public void setLivroSelecionado(LivroDTO livroSelecionado) {
        this.livroSelecionado = livroSelecionado;
    }

    /**
     * @return the reservaSelecionada
     */
    public ReservaDTO getReservaSelecionada() {
        return reservaSelecionada;
    }

    /**
     * @param reservaSelecionada the reservaSelecionada to set
     */
    public void setReservaSelecionada(ReservaDTO reservaSelecionada) {
        this.reservaSelecionada = reservaSelecionada;
    }

    /**
     * @return the usuarioSelecionado
     */
    public UsuarioDTO getUsuarioSelecionado() {
        return usuarioSelecionado;
    }

    /**
     * @param usuarioSelecionado the usuarioSelecionado to set
     */
    public void setUsuarioSelecionado(UsuarioDTO usuarioSelecionado) {
        this.usuarioSelecionado = usuarioSelecionado;
    }

    /**
     * @return the usuarioFilter
     */
    public UsuarioFilter getUsuarioFilter() {
        return usuarioFilter;
    }

    /**
     * @param usuarioFilter the usuarioFilter to set
     */
    public void setUsuarioFilter(UsuarioFilter usuarioFilter) {
        this.usuarioFilter = usuarioFilter;
    }
}
