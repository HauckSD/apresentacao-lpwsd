/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teste.lpwsd.dao;
import com.teste.lpwsd.bd.PersistenceUtil;
import com.teste.lpwsd.dao.GenericDao;
import com.teste.lpwsd.filter.UsuarioFilter;
import com.teste.lpwsd.modelo.TbLivro;
import com.teste.lpwsd.modelo.TbUsuario;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author celio
 */
public class UsuarioDao extends GenericDao<TbUsuario, Integer> {
      
    public UsuarioDao() {
        super(TbUsuario.class);
    }
    
    public TbUsuario getUsuario(String nomeUsuario, String senha) {
  
            try {
                
                String sql = "SELECT u from TbUsuario u where u.nomeUsuario = :name and u.senha = :senha";
                EntityManager manager = PersistenceUtil.getEntityManager();
                
                Query query = manager.createQuery(sql);
                query.setParameter("name", nomeUsuario);
                query.setParameter("senha", senha);
                
                  TbUsuario usuario = (TbUsuario) query.getSingleResult();
  
                  return usuario;
            } catch (NoResultException e) {
                  return null;
            }
      }
    
        public TbUsuario getUsuario(UsuarioFilter filter) {
  
            try {
                
                String sql = "SELECT u from TbUsuario u where u.nomeUsuario = :name ";
                EntityManager manager = PersistenceUtil.getEntityManager();
                
                Query query = manager.createQuery(sql);
                query.setParameter("name", filter.getNome());
                
                  TbUsuario usuario = (TbUsuario) query.getSingleResult();
  
                  return usuario;
            } catch (NoResultException e) {
                  return null;
            }
      }
}
