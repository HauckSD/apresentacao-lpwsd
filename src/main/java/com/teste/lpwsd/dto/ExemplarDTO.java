/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teste.lpwsd.dto;

/**
 *
 * @author chjunior1
 */
public class ExemplarDTO {
    
    private Integer idExemplar;
	
    private Integer circular;
	
    private LivroDTO livroDTO;
    
    public ExemplarDTO() {
    
    }
    
    public ExemplarDTO(Integer idExemplar){
        this.idExemplar = idExemplar;
    }

    /**
     * @return the idExemplar
     */
    public Integer getIdExemplar() {
        return idExemplar;
    }

    /**
     * @param idExemplar the idExemplar to set
     */
    public void setIdExemplar(Integer idExemplar) {
        this.idExemplar = idExemplar;
    }

    /**
     * @return the circular
     */
    public Integer getCircular() {
        return circular;
    }

    /**
     * @param circular the circular to set
     */
    public void setCircular(Integer circular) {
        this.circular = circular;
    }

    /**
     * @return the livroDTO
     */
    public LivroDTO getLivroDTO() {
        return livroDTO;
    }

    /**
     * @param livroDTO the livroDTO to set
     */
    public void setLivroDTO(LivroDTO livroDTO) {
        this.livroDTO = livroDTO;
    }

}
