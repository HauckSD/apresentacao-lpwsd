/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teste.lpwsd.bean;

import com.teste.lpwsd.dao.EditoraDao;;
import com.teste.lpwsd.modelo.TbEditora;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

/**
 *
 * @author celio
 */
@ManagedBean(name = "EditoraBean")
@ViewScoped()
public class EditoraBean implements Serializable {
    private TbEditora editora = new TbEditora();
    EditoraDao editoraDao;
    private List editoras = new ArrayList();
    
    public EditoraBean(){
        editoraDao =  new EditoraDao();
        editoras = editoraDao.buscarTodos();
    }
    
        //Métodos dos botões 
    public void record(ActionEvent actionEvent) {
        editoraDao.atualizar(this.getEditora());
        setEditoras(editoraDao.buscarTodos());
        setEditora(new TbEditora());
    }

    public void exclude(ActionEvent actionEvent) {
        editoraDao.remover(getEditora().getIdtbEditora());
        setEditoras(editoraDao.buscarTodos());
        setEditora(new TbEditora());
    }

    /**
     * @return the editora
     */
    public TbEditora getEditora() {
        return editora;
    }

    /**
     * @param editora the editora to set
     */
    public void setEditora(TbEditora editora) {
        this.editora = editora;
    }

    /**
     * @return the editoras
     */
    public List getEditoras() {
        return editoras;
    }

    /**
     * @param editoras the editoras to set
     */
    public void setEditoras(List editoras) {
        this.editoras = editoras;
    }
    
    
}
