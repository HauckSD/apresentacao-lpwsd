/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teste.lpwsd.dao;

import com.teste.lpwsd.modelo.TbEditora;

/**
 *
 * @author celio
 */
public class EditoraDao extends GenericDao<TbEditora, Integer> {
    public EditoraDao(){
        super(TbEditora.class);
    }
    
}
