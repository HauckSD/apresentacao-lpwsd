/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teste.lpwsd.dao;

import com.teste.lpwsd.dto.EmprestimoDTO;
import com.teste.lpwsd.dto.ExemplarDTO;
import com.teste.lpwsd.dto.LivroDTO;
import com.teste.lpwsd.dto.UsuarioDTO;
import com.teste.lpwsd.modelo.TbAutores;

import com.teste.lpwsd.modelo.TbEmprestimo;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Query;


/**
 *
 * @author chjunior1
 */

public class EmprestimoDao extends GenericDao<TbEmprestimo, Integer> {
    
    private UsuarioDao usuarioDao;
    
    private ExemplarDao exemplarDao;
    
    public EmprestimoDao() {
       super(TbEmprestimo.class);
       this.usuarioDao = new UsuarioDao();
       this.exemplarDao = new ExemplarDao();
    }



	public void salvarEmprestimo(EmprestimoDTO emprestimoDTO) {
		TbEmprestimo emprestimo = new TbEmprestimo();
		//TbExemplar exemplarEntity = new TbExemplar();
		//UsuarioEntity usuarioEntity = new UsuarioEntity();
		
		emprestimo.setTbExemplaridtbExemplar(exemplarDao.buscarPorId(emprestimoDTO.getLivroDTO().getIdExemplar()));
		emprestimo.setTbUsuarioidtbUsuario(usuarioDao.buscarPorId(Math.toIntExact(emprestimoDTO.getUsuarioDTO().getIdUsuario())));
		emprestimo.setDataEmprestimo(new Date());
		emprestimo.setDataDevolucaoPrevista(emprestimoDTO.getDataDevolucao());
		this.salvar(emprestimo);
		
		
	}
	@SuppressWarnings("unchecked")
	public List<EmprestimoDTO> obterEmprestimos() {
            
		List<EmprestimoDTO> emprestimos = new ArrayList<EmprestimoDTO>();
                
		String sql = this.criarSqlEmprestimo();
                
		List<Object[]> results = super.getManager().createNativeQuery(sql).getResultList();
		
		results.stream().forEach((result) -> {
                        EmprestimoDTO emprestimo = new EmprestimoDTO();
			montarEmprestimo(emprestimo, result);
                        montarExemplar(emprestimo, result);
                        montarLivro(emprestimo, result);
                        montarUsuario(emprestimo, result);

	        emprestimos.add(emprestimo);
		});
		
		return emprestimos;
	}
        
        @SuppressWarnings("unchecked")
	public List<EmprestimoDTO> obterEmprestimosAlugados(Long id) {
            
		List<EmprestimoDTO> emprestimos = new ArrayList<EmprestimoDTO>();
                
		String sql = this.criarSqlEmprestimoSemEmprestimo();
                
                
                 Query query = super.getManager().createNativeQuery(sql);
                query.setParameter("exemplar", id);
		List<Object[]> results = query.getResultList();
		
		results.stream().forEach((result) -> {
                        EmprestimoDTO emprestimo = new EmprestimoDTO();
			montarEmprestimo(emprestimo, result);
                        montarExemplar(emprestimo, result);
                        montarLivro(emprestimo, result);
                        montarUsuario(emprestimo, result);

	        emprestimos.add(emprestimo);
		});
		
		return emprestimos;
	}
        
        private void montarEmprestimo(EmprestimoDTO emprestimo, Object[] result) {
                        emprestimo.setIdEmprestimo((Integer) result[0]);
			emprestimo.setDataEmprestimo((Date) result[1]);
			emprestimo.setDataDevolucao((Date) result[2]);
        }
        
        private void montarLivro(EmprestimoDTO emprestimo, Object[] result) {
                emprestimo.setLivroDTO(new LivroDTO((String) result[4], 
                                                    (Integer) result[5],
                                                    (String) result[6]));
        }
        
        private void montarUsuario(EmprestimoDTO emprestimo, Object[] result) {
            emprestimo.setUsuarioDTO(new UsuarioDTO((String) result[7]));
        }
        
        private void montarExemplar(EmprestimoDTO emprestimo, Object[] result) {
            emprestimo.setExemplarDTO(new ExemplarDTO((Integer) result[3]));
        }
        private String criarSqlEmprestimo() {
            return " SELECT EMP.idtbEmprestimo, "
                    + " EMP.dataDevolucao, "
                    + " EMP.dataEmprestimo, "
                    + " EXE.idtbExemplar as exemplar_id, "
                    + " LIV.titulo, "
                    + " LIV.edicao, "
                    + " LIV.isbn, "
                    + " USU.nomeUsuario "
                    + " FROM tbemprestimo AS EMP "
                    + " INNER JOIN tbexemplar AS EXE ON EMP.tbExemplar_idtbExemplar = EXE.idtbExemplar "
                    + " INNER JOIN tblivro AS LIV ON EXE.tbLivro_idtbLivro = LIV.idtbLivro "
                    + " INNER JOIN tbusuario AS USU ON EMP.tbUsuario_idtbUsuario = USU.idtbUsuario ";
        }
        
       private String criarSqlEmprestimoSemEmprestimo() {
            return  " SELECT EMP.idtbEmprestimo, "
                    + " EMP.dataDevolucao, "
                    + " EMP.dataEmprestimo, "
                    + " EXE.idtbExemplar as exemplar_id, "
                    + " LIV.titulo, "
                    + " LIV.edicao, "
                    + " LIV.isbn, "
                    + " USU.nomeUsuario "
                    + " FROM tbemprestimo AS EMP "
                    + " INNER JOIN tbexemplar AS EXE ON EMP.tbExemplar_idtbExemplar = EXE.idtbExemplar "
                    + " INNER JOIN tblivro AS LIV ON EXE.tbLivro_idtbLivro = LIV.idtbLivro "
                    + " INNER JOIN tbusuario AS USU ON EMP.tbUsuario_idtbUsuario = USU.idtbUsuario "
                    + " where EMP.dataDevolucao is null and EXE.idtbExemplar = :exemplar";
        }
}
