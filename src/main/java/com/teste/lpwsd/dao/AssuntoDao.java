/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teste.lpwsd.dao;

import com.teste.lpwsd.modelo.TbAssunto;

/**
 *
 * @author celio
 */
public class AssuntoDao extends GenericDao<TbAssunto, Integer>{
       public AssuntoDao(){
        super(TbAssunto.class);
    }
}
