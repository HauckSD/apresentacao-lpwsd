/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teste.lpwsd.converter;

import com.teste.lpwsd.dto.UsuarioDTO;
import com.teste.lpwsd.modelo.TbUsuario;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author celio
 */
public class UsuarioConveter {
    
    public static UsuarioDTO converter(TbUsuario usuario) {
      UsuarioDTO usuarioDTO = new UsuarioDTO();
      usuarioDTO.setIdUsuario(usuario.getIdtbUsuario().longValue());
      usuarioDTO.setSenha(usuario.getSenha());
      usuarioDTO.setEmail(usuario.getEmail());
      usuarioDTO.setUsuario(usuario.getNomeUsuario());
      usuarioDTO.setTipo(usuario.getTipoUsuario());
      return usuarioDTO;
    }
    
    public static List<UsuarioDTO> converterList(List<TbUsuario> usuarios) {
    return usuarios.stream().map(n -> converter(n)).collect(Collectors.toList());
    }
    
    
    
    
    
}
