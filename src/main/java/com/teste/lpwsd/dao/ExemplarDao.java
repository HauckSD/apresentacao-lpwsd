/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teste.lpwsd.dao;

import com.teste.lpwsd.dto.AssuntoDTO;
import com.teste.lpwsd.dto.EditoraDTO;
import com.teste.lpwsd.dto.LivroDTO;
import com.teste.lpwsd.modelo.TbExemplar;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author celio
 */
public class ExemplarDao extends GenericDao<TbExemplar, Integer> {
    public ExemplarDao(){
        super(TbExemplar.class);
    }
    
    	@SuppressWarnings("unchecked")
	public List<LivroDTO> obterExemplares() {
		List<LivroDTO> livros = new ArrayList<>();
		
		StringBuilder hql = new StringBuilder();
		hql.append("SELECT    DISTINCT 	exemplar.idtbExemplar as cod_exemplar," + 
				"            exemplar.circular AS circular, " + 
				"            livro.titulo      AS titulo, " + 
				"            livro.ano         AS ano, " + 
				"            livro.edicao      AS edicao, " + 
				"            livro.isbn        AS isbn, " + 
				"            livro.idtbLivro          AS id_livro, " + 
				"            edi.nomeEditora 	  	   AS editora, " + 
				"            assu.assunto	   AS Categoria " + 
				"	FROM     tbexemplar       AS exemplar " + 
				"	INNER JOIN  tblivro                   AS livro ON exemplar.tbLivro_idtbLivro = livro.idtbLivro " + 
				"	INNER JOIN tbeditora                  AS edi  ON livro.tbEditora_idtbEditora = edi.idtbEditora " + 
				"	LEFT JOIN  tbassunto AS assu  ON assu.idtbAssunto = livro.tbAssunto_idtbAssunto ");
		
		List<Object[]> results = this.getManager().createNativeQuery(hql.toString()).getResultList();
		
		results.stream().forEach((response) -> {
			
			EditoraDTO editoraModel = new EditoraDTO();
			AssuntoDTO assuntoModel = new AssuntoDTO();
			LivroDTO livro = new LivroDTO();
	        
		livro.setIdExemplar((Integer) response[0]);
	        livro.setCicurlar(((Short) response[1]) !=0 );
	        livro.setTitulo((String) response[2]);
	        livro.setAno(recuperarAno((Date) response[3]));
	        livro.setEdicao((Integer) response[4]);
	        livro.setIsbn((String) response[5]);
	        livro.setIdLivro((Integer) response[6]);
	        	editoraModel.setNome((String) response[7]);
	        livro.setEditoraDTO(editoraModel);
	        	assuntoModel.setAssunto((String) response[8]);
	        livro.setAssuntoDTO(assuntoModel);
	        
	        livros.add(livro);
		});
		
		return livros;
	}
        
        private int recuperarAno(Date date) {
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(date);
        return calendar1.get(Calendar.YEAR);
        }
    
}
