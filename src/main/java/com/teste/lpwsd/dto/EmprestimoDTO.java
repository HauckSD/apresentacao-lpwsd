/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teste.lpwsd.dto;

import java.util.Date;

/**
 *
 * @author chjunior1
 */
public class EmprestimoDTO {
    	
	private Integer idEmprestimo;
	private Date dataEmprestimo;
	private Date dataDevolucao;
	private ExemplarDTO exemplarDTO;
	private UsuarioDTO usuarioDTO;
	private LivroDTO livroDTO;
        
        public EmprestimoDTO () {
        
        }
        
         public EmprestimoDTO (LivroDTO livroDTO, UsuarioDTO usuarioDTO, 
                               Date dataDevolucao) {
             this.livroDTO = livroDTO;
             this.usuarioDTO = usuarioDTO;
             this.dataDevolucao = dataDevolucao;
        }
 
    /**
     * @return the idEmprestimo
     */
    public Integer getIdEmprestimo() {
        return idEmprestimo;
    }

    /**
     * @param idEmprestimo the idEmprestimo to set
     */
    public void setIdEmprestimo(Integer idEmprestimo) {
        this.idEmprestimo = idEmprestimo;
    }

    /**
     * @return the dataEmprestimo
     */
    public Date getDataEmprestimo() {
        return dataEmprestimo;
    }

    /**
     * @param dataEmprestimo the dataEmprestimo to set
     */
    public void setDataEmprestimo(Date dataEmprestimo) {
        this.dataEmprestimo = dataEmprestimo;
    }

    /**
     * @return the dataDevolucao
     */
    public Date getDataDevolucao() {
        return dataDevolucao;
    }

    /**
     * @param dataDevolucao the dataDevolucao to set
     */
    public void setDataDevolucao(Date dataDevolucao) {
        this.dataDevolucao = dataDevolucao;
    }

    /**
     * @return the exemplarDTO
     */
    public ExemplarDTO getExemplarDTO() {
        return exemplarDTO;
    }

    /**
     * @param exemplarDTO the exemplarDTO to set
     */
    public void setExemplarDTO(ExemplarDTO exemplarDTO) {
        this.exemplarDTO = exemplarDTO;
    }

    /**
     * @return the usuarioDTO
     */
    public UsuarioDTO getUsuarioDTO() {
        return usuarioDTO;
    }

    /**
     * @param usuarioDTO the usuarioDTO to set
     */
    public void setUsuarioDTO(UsuarioDTO usuarioDTO) {
        this.usuarioDTO = usuarioDTO;
    }

    /**
     * @return the livroDTO
     */
    public LivroDTO getLivroDTO() {
        return livroDTO;
    }

    /**
     * @param livroDTO the livroDTO to set
     */
    public void setLivroDTO(LivroDTO livroDTO) {
        this.livroDTO = livroDTO;
    }
        
        
}
