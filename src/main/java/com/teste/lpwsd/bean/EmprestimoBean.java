/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teste.lpwsd.bean;

import com.teste.lpwsd.converter.UsuarioConveter;
import com.teste.lpwsd.dao.AssuntoDao;
import com.teste.lpwsd.dao.EmprestimoDao;
import com.teste.lpwsd.dao.ExemplarDao;
import com.teste.lpwsd.dao.LivroDao;
import com.teste.lpwsd.dao.UsuarioDao;
import com.teste.lpwsd.dominio.TipoUsuario;
import com.teste.lpwsd.dto.EmprestimoDTO;
import com.teste.lpwsd.dto.ExemplarDTO;
import com.teste.lpwsd.dto.LivroDTO;
import com.teste.lpwsd.dto.UsuarioDTO;
import com.teste.lpwsd.filter.ExemplarFilter;
import com.teste.lpwsd.filter.UsuarioFilter;
import com.teste.lpwsd.modelo.TbAssunto;
import com.teste.lpwsd.modelo.TbEmprestimo;
import com.teste.lpwsd.modelo.TbExemplar;
import com.teste.lpwsd.modelo.TbLivro;
import com.teste.lpwsd.modelo.TbUsuario;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author chjunior1
 */
@ManagedBean(name = "emprestimoBean")
@ViewScoped()
public class EmprestimoBean implements Serializable{

    /**
     * @return the filter
     */
    public UsuarioFilter getFilter() {
        return filter;
    }

    /**
     * @param filter the filter to set
     */
    public void setFilter(UsuarioFilter filter) {
        this.filter = filter;
    }
    
    	private static final long serialVersionUID = 1L;

	private LivroDTO livroDTO = new LivroDTO();
        
        private LivroDTO livroSelecionado = new LivroDTO();
                
	private LivroDao livroDao;
        
	private List<LivroDTO> livros;


	private UsuarioDao usuarioDao;
        
	private UsuarioDTO usuarioDTO;
        
        private UsuarioFilter filter;
        
        private ExemplarDao exemplarDao;
        
	private List<UsuarioDTO> usuarios;
	
	private EmprestimoDao emprestimoDao;
        
        private EmprestimoDTO emprestimoSelecionado;
        
        private ExemplarFilter exemplarFilter;
        
	private List<LivroDTO> emprestimos = new ArrayList<>();
        
	List<EmprestimoDTO> emprestimosConsulta = new ArrayList<>();
        
        private List<EmprestimoDTO> emprestimosPendentes = new ArrayList<>();
	
	
	private UsuarioDTO usuarioSelecionado = new UsuarioDTO();
	private boolean possuiPermissaoParaLocar = true;
	private Date dataDevolucao = new Date();
	private Date dataReserva;
	
	public EmprestimoBean() {
            this.Instanciar();
            this.setLivros(this.exemplarDao.obterExemplares());
            this.setUsuarios(UsuarioConveter.converterList(this.usuarioDao.buscarTodos()));
            this.setEmprestimosConsulta(this.emprestimoDao.obterEmprestimos());
	}
        
        private void Instanciar() {
            this.livroDTO = new LivroDTO();
            this.usuarioDao = new UsuarioDao();
            this.filter = new UsuarioFilter();
            this.setExemplarDao(new ExemplarDao());
            this.setEmprestimoSelecionado(new EmprestimoDTO());
            this.livroDao = new LivroDao();
            this.livros = new ArrayList<>();
            this.usuarioDTO = new UsuarioDTO();
            this.setExemplarFilter(new ExemplarFilter());
            this.usuarios = new ArrayList<>();
            this.emprestimoDao = new EmprestimoDao();            
            
        }
        
        public void buscarEmprestimosPendentes() {
        String[] array = this.exemplarFilter.getCodigo().split("-");
        Long id = Long.parseLong(array[0].trim().replaceAll("\\s",""));
        this.exemplarFilter.setId(id);
        this.emprestimosPendentes = emprestimoDao.obterEmprestimosAlugados(exemplarFilter.getId());
        }
        
        public void devolver() {
        TbEmprestimo emprestimo = emprestimoDao.buscarPorId(emprestimoSelecionado.getIdEmprestimo());
        if(emprestimo.getDataDevolucaoPrevista().before(new Date())) {
            this.mensagemErro("Prazo invalido");
        } else {
        emprestimo.setDataDevolucao(new Date());
        this.emprestimoDao.atualizar(emprestimo);
        this.buscarEmprestimosPendentes();
        }

        }

        public void alocarLivro(LivroDTO livroDTO) {
            this.getEmprestimos().add(livroDTO);
            this.setLivroDTO(livroDTO);
	}
        

        
//        public void devolverExemplar() {
//            if(this.livroDTO.get)
//        }
        
	
	public void salvarEmprestimo() {
             System.out.println("com.teste.lpwsd.bean.EmprestimoBean.alocarLivro()");
             this.usuarioSelecionado = UsuarioConveter.converter(this.usuarioDao.getUsuario(filter));
		if(this.getLivroDTO().getIdLivro() != null && validarEmprestimo() && verificarExemplar()) {
                    this.emprestimoDao.salvarEmprestimo(new EmprestimoDTO(this.getLivroDTO(), this.getUsuarioSelecionado(), this.getDataDevolucao()));
                    this.setLivroDTO(null);
                    this.setDataDevolucao(null);
		}
	}
        
        private boolean validarEmprestimo() {
            TbUsuario tbUsuario = this.usuarioDao
                                      .buscarPorId(
                                       this.usuarioSelecionado
                                      .getIdUsuario().intValue());
            
            return !validarUsuarioDebito(tbUsuario) && !validarQuantidadeMaximaLivros(tbUsuario);
        }
        
        private boolean validarUsuarioDebito(TbUsuario tbUsuario) {
            List<TbEmprestimo> emprestimos = tbUsuario.getTbEmprestimoList()
                                                       .stream()
                                                       .filter(d -> {
                                                           if(Objects.nonNull(d.getDataDevolucao()) && d.getDataDevolucaoPrevista().before(d.getDataDevolucao())) {
                                                                return true;
                                                           }
                                                           return false;
                                                       })
                                                       .collect(Collectors.toList());
            
            if(emprestimos.size() > 0) {
                mensagemErro("Usuario esta em debito");
               return true;
            } else {
                return false;
            }
        }
        
        private boolean validarQuantidadeMaximaLivros(TbUsuario tbUsuario) {
             int quantidadeTotalEmprestimosAtivos = tbUsuario.getTbEmprestimoList()
                                                             .stream()
                                                             .filter(d -> {
                                                                 return d.getDataEmprestimo() != null;
                                                             })
                                                             .collect(Collectors.toList())
                                                             .size();
             return verificarQuantidadeMaxima(quantidadeTotalEmprestimosAtivos, tbUsuario.getTipoUsuario());
        }
        
        private boolean verificarQuantidadeMaxima(int quantidade, String tipoUsuario) {
            if(TipoUsuario.ALUNO.getTipo().equals(tipoUsuario) || TipoUsuario.FUNCIONARIO.getTipo().equals(tipoUsuario)) {
                boolean q =  quantidade > 3;
                if(q) {
                    mensagemErro("Quantidade Maxima atiginda");
                }
                return q;
            }
            
            if(TipoUsuario.PROFESSOR.getTipo().equals(tipoUsuario)) { 
                boolean q = quantidade > 5;
                if(q) {
                    mensagemErro("Quantidade Maxima atiginda");
                }
                return q;
            }
            
            return false;
        }
        
        private boolean verificarExemplar() {
           TbExemplar exemplar = this.getExemplarDao().buscarPorId(this.getLivroDTO().getIdExemplar());
           boolean q = verificarDisponibilidadeExemplar(exemplar);
           if(q) {
           calcularDataEmprestimo(exemplar);
           }
           return q;
        }
        
        private boolean verificarDisponibilidadeExemplar(TbExemplar exemplar) {
           boolean q = exemplar.getTbEmprestimoList()
                    .stream()
                    .filter(d -> {return d.getDataDevolucao()== null;})
                    .collect(Collectors.toList())
                    .size() == 0;
           if(!q) {
               mensagemErro("Exemplar Indisponivel");
           }
           return q;
        }
        
        private void calcularDataEmprestimo(TbExemplar exemplar) {
            this.dataDevolucao = new Date();
            if(exemplar.getCircular() == 1){
            if(TipoUsuario.ALUNO.getTipo().equals(usuarioSelecionado.getTipo()) || TipoUsuario.FUNCIONARIO.getTipo().equals(usuarioSelecionado.getTipo())) {
                this.dataDevolucao.setHours(24*10);
            }
            
            if(TipoUsuario.PROFESSOR.getTipo().equals(usuarioSelecionado.getTipo())) { 
                this.dataDevolucao.setHours(24*15);
            }
  
        }
        }
        
        private void mensagemErro(String mensagem){
            FacesContext.getCurrentInstance().addMessage(
                             null,
                             new FacesMessage(FacesMessage.SEVERITY_ERROR, mensagem,
                                         "Erro Regra de Negocio!"));
        }

    /**
     * @return the livroDTO
     */
    public LivroDTO getLivroDTO() {
        return livroDTO;
    }

    /**
     * @param livroDTO the livroDTO to set
     */
    public void setLivroDTO(LivroDTO livroDTO) {
        this.livroDTO = livroDTO;
    }

    public LivroDao getLivroDao() {
        return livroDao;
    }


    public void setLivroDao(LivroDao livroRepository) {
        this.livroDao = livroRepository;
    }

    /**
     * @return the livros
     */
    public List<LivroDTO> getLivros() {
        return livros;
    }

    /**
     * @param livros the livros to set
     */
    public void setLivros(List<LivroDTO> livros) {
        this.livros = livros;
    }

    public UsuarioDao getUsuarioDao() {
        return usuarioDao;
    }

    public void setUsuarioDao(UsuarioDao usuarioDao) {
        this.usuarioDao = usuarioDao;
    }

    /**
     * @return the usuarioDTO
     */
    public UsuarioDTO getUsuarioDTO() {
        return usuarioDTO;
    }

    /**
     * @param usuarioDTO the usuarioDTO to set
     */
    public void setUsuarioDTO(UsuarioDTO usuarioDTO) {
        this.usuarioDTO = usuarioDTO;
    }

    /**
     * @return the usuarios
     */
    public List<UsuarioDTO> getUsuarios() {
        return usuarios;
    }

    /**
     * @param usuarios the usuarios to set
     */
    public void setUsuarios(List<UsuarioDTO> usuarios) {
        this.usuarios = usuarios;
    }

    /**
     * @return the emprestimoRepository
     */
    public EmprestimoDao getEmprestimoDao() {
        return emprestimoDao;
    }

    public void setEmprestimoDao(EmprestimoDao emprestimoDao) {
        this.emprestimoDao = emprestimoDao;
    }

    /**
     * @return the emprestimos
     */
    public List<LivroDTO> getEmprestimos() {
        return emprestimos;
    }

    /**
     * @param emprestimos the emprestimos to set
     */
    public void setEmprestimos(List<LivroDTO> emprestimos) {
        this.emprestimos = emprestimos;
    }

    /**
     * @return the emprestimosConsulta
     */
    public List<EmprestimoDTO> getEmprestimosConsulta() {
        return emprestimosConsulta;
    }

    /**
     * @param emprestimosConsulta the emprestimosConsulta to set
     */
    public void setEmprestimosConsulta(List<EmprestimoDTO> emprestimosConsulta) {
        this.emprestimosConsulta = emprestimosConsulta;
    }

    /**
     * @return the usuarioSelecionado
     */
    public UsuarioDTO getUsuarioSelecionado() {
        return usuarioSelecionado;
    }

    /**
     * @param usuarioSelecionado the usuarioSelecionado to set
     */
    public void setUsuarioSelecionado(UsuarioDTO usuarioSelecionado) {
        this.usuarioSelecionado = usuarioSelecionado;
    }

    /**
     * @return the possuiPermissaoParaLocar
     */
    public boolean isPossuiPermissaoParaLocar() {
        return possuiPermissaoParaLocar;
    }

    /**
     * @param possuiPermissaoParaLocar the possuiPermissaoParaLocar to set
     */
    public void setPossuiPermissaoParaLocar(boolean possuiPermissaoParaLocar) {
        this.possuiPermissaoParaLocar = possuiPermissaoParaLocar;
    }

    /**
     * @return the dataDevolucao
     */
    public Date getDataDevolucao() {
        return dataDevolucao;
    }

    /**
     * @param dataDevolucao the dataDevolucao to set
     */
    public void setDataDevolucao(Date dataDevolucao) {
        this.dataDevolucao = dataDevolucao;
    }

    /**
     * @return the dataReserva
     */
    public Date getDataReserva() {
        return dataReserva;
    }

    /**
     * @param dataReserva the dataReserva to set
     */
    public void setDataReserva(Date dataReserva) {
        this.dataReserva = dataReserva;
    }

    /**
     * @return the livroSelecionado
     */
    public LivroDTO getLivroSelecionado() {
        return livroSelecionado;
    }

    /**
     * @param livroSelecionado the livroSelecionado to set
     */
    public void setLivroSelecionado(LivroDTO livroSelecionado) {
        this.livroSelecionado = livroSelecionado;
    }

    /**
     * @return the exemplarDao
     */
    public ExemplarDao getExemplarDao() {
        return exemplarDao;
    }

    /**
     * @param exemplarDao the exemplarDao to set
     */
    public void setExemplarDao(ExemplarDao exemplarDao) {
        this.exemplarDao = exemplarDao;
    }


    /**
     * @return the emprestimosPendentes
     */
    public List<EmprestimoDTO> getEmprestimosPendentes() {
        return emprestimosPendentes;
    }

    /**
     * @param emprestimosPendentes the emprestimosPendentes to set
     */
    public void setEmprestimosPendentes(List<EmprestimoDTO> emprestimosPendentes) {
        this.emprestimosPendentes = emprestimosPendentes;
    }

    /**
     * @return the exemplarFilter
     */
    public ExemplarFilter getExemplarFilter() {
        return exemplarFilter;
    }

    /**
     * @param exemplarFilter the exemplarFilter to set
     */
    public void setExemplarFilter(ExemplarFilter exemplarFilter) {
        this.exemplarFilter = exemplarFilter;
    }

    /**
     * @return the emprestimoSelecionado
     */
    public EmprestimoDTO getEmprestimoSelecionado() {
        return emprestimoSelecionado;
    }

    /**
     * @param emprestimoSelecionado the emprestimoSelecionado to set
     */
    public void setEmprestimoSelecionado(EmprestimoDTO emprestimoSelecionado) {
        this.emprestimoSelecionado = emprestimoSelecionado;
    }
	
}
