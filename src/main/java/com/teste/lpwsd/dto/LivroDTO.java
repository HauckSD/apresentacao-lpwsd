/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teste.lpwsd.dto;

/**
 *
 * @author chjunior1
 */
public class LivroDTO {
    	private Integer idLivro;
        
        private Integer idExemplar;
	
	private String titulo;
	
	private String isbn;
	
	private Integer edicao;
	
	private Integer ano;
		
	private boolean cicurlar;
        
        private EditoraDTO editoraDTO;
        
        private AssuntoDTO assuntoDTO;
        
        public LivroDTO() {
        
        }
        
        public LivroDTO(String titulo, Integer edicao, String isbn) {
            this.titulo = titulo;
            this.edicao = edicao;
            this.isbn = isbn;
        }

    /**
     * @return the idLivro
     */
    public Integer getIdLivro() {
        return idLivro;
    }

    /**
     * @param idLivro the idLivro to set
     */
    public void setIdLivro(Integer idLivro) {
        this.idLivro = idLivro;
    }

    /**
     * @return the titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return the isbn
     */
    public String getIsbn() {
        return isbn;
    }

    /**
     * @param isbn the isbn to set
     */
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    /**
     * @return the edicao
     */
    public Integer getEdicao() {
        return edicao;
    }

    /**
     * @param edicao the edicao to set
     */
    public void setEdicao(Integer edicao) {
        this.edicao = edicao;
    }

    /**
     * @return the ano
     */
    public Integer getAno() {
        return ano;
    }

    /**
     * @param ano the ano to set
     */
    public void setAno(Integer ano) {
        this.ano = ano;
    }

    /**
     * @return the cicurlar
     */
    public boolean getCicurlar() {
        return cicurlar;
    }

    /**
     * @param cicurlar the cicurlar to set
     */
    public void setCicurlar(boolean cicurlar) {
        this.cicurlar = cicurlar;
    }

    /**
     * @return the idExemplar
     */
    public Integer getIdExemplar() {
        return idExemplar;
    }

    /**
     * @param idExemplar the idExemplar to set
     */
    public void setIdExemplar(Integer idExemplar) {
        this.idExemplar = idExemplar;
    }

    /**
     * @return the editoraDTO
     */
    public EditoraDTO getEditoraDTO() {
        return editoraDTO;
    }

    /**
     * @param editoraDTO the editoraDTO to set
     */
    public void setEditoraDTO(EditoraDTO editoraDTO) {
        this.editoraDTO = editoraDTO;
    }

    /**
     * @return the assuntoDTO
     */
    public AssuntoDTO getAssuntoDTO() {
        return assuntoDTO;
    }

    /**
     * @param assuntoDTO the assuntoDTO to set
     */
    public void setAssuntoDTO(AssuntoDTO assuntoDTO) {
        this.assuntoDTO = assuntoDTO;
    }
        
        
}
