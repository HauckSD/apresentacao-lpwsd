/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teste.lpwsd.dto;

import java.util.Date;

/**
 *
 * @author chjunior1
 */
public class ReservaDTO {
    	private Integer idReserva;
	private Date dataReserva;
	private ExemplarDTO exemplarDTO;
	private UsuarioDTO usuarioDTO;
	private EmprestimoDTO emprestimoDTO;

    /**
     * @return the idReserva
     */
    public Integer getIdReserva() {
        return idReserva;
    }

    /**
     * @param idReserva the idReserva to set
     */
    public void setIdReserva(Integer idReserva) {
        this.idReserva = idReserva;
    }

    /**
     * @return the dataReserva
     */
    public Date getDataReserva() {
        return dataReserva;
    }

    /**
     * @param dataReserva the dataReserva to set
     */
    public void setDataReserva(Date dataReserva) {
        this.dataReserva = dataReserva;
    }

    /**
     * @return the exemplarDTO
     */
    public ExemplarDTO getExemplarDTO() {
        return exemplarDTO;
    }

    /**
     * @param exemplarDTO the exemplarDTO to set
     */
    public void setExemplarDTO(ExemplarDTO exemplarDTO) {
        this.exemplarDTO = exemplarDTO;
    }

    /**
     * @return the usuarioDTO
     */
    public UsuarioDTO getUsuarioDTO() {
        return usuarioDTO;
    }

    /**
     * @param usuarioDTO the usuarioDTO to set
     */
    public void setUsuarioDTO(UsuarioDTO usuarioDTO) {
        this.usuarioDTO = usuarioDTO;
    }

    /**
     * @return the emprestimoDTO
     */
    public EmprestimoDTO getEmprestimoDTO() {
        return emprestimoDTO;
    }

    /**
     * @param emprestimoDTO the emprestimoDTO to set
     */
    public void setEmprestimoDTO(EmprestimoDTO emprestimoDTO) {
        this.emprestimoDTO = emprestimoDTO;
    }
        
        
}
