/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teste.lpwsd.bean;

import com.teste.lpwsd.dao.AssuntoDao;
import com.teste.lpwsd.dao.AutorDao;
import com.teste.lpwsd.dao.LivroDao;
import com.teste.lpwsd.dto.ConsultaAcervoDTO;
import com.teste.lpwsd.filter.AcervoFilter;
import com.teste.lpwsd.modelo.TbAssunto;
import com.teste.lpwsd.modelo.TbAutores;
import com.teste.lpwsd.modelo.TbExemplar;
import com.teste.lpwsd.modelo.TbLivro;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author celio
 */
@ManagedBean(name = "AcervoBean")
@ViewScoped()
public class AcervoBean  implements Serializable{
    private List<ConsultaAcervoDTO> acervos = new ArrayList<ConsultaAcervoDTO>();
    private AcervoFilter filtro;
    AutorDao autorDao;
    AssuntoDao assuntoDao;
    LivroDao livroDao;
    private List<TbAutores> autoresDb = new ArrayList();
    private List<TbAssunto> assuntosDb = new ArrayList();
    private List<TbLivro> livrosDb = new ArrayList();
    
    public AcervoBean() {
    filtro = new AcervoFilter();
    autorDao = new AutorDao();
    livroDao = new LivroDao();
    assuntoDao = new AssuntoDao();
    autoresDb = autorDao.buscarTodos();
    assuntosDb = assuntoDao.buscarTodos();
    }
    
    
    public void consultarAcervos(){
        this.acervos = new ArrayList<>();
        this.tratarFiltro(filtro);
        this.livrosDb = livroDao.buscarAcervo(filtro);
        this.adicionarCriterios();
    }
    
    private void adicionarCriterios() {
       this.livrosDb.stream().forEach(l -> 
       {
           if(this.acervos.stream().filter(n -> n.getTitulo().equals(l.getTitulo())).collect(Collectors.toList()).size() == 0) {
                this.acervos.add(new ConsultaAcervoDTO(l.getTitulo(),l.getTbExemplarList().size() ,
                            quantidadeDisponivelIndisponivel(l.getTbExemplarList(),1),quantidadeDisponivelIndisponivel(l.getTbExemplarList(),0)));
           }
          
       });
    }
    
    private void tratarFiltro(AcervoFilter filtro) {
        
        if(filtro.getAutor().equals("")) {
            filtro.setAutor(null);
        }
        
         if(filtro.getAssunto().equals("")) {
            filtro.setAssunto(null);
        }
                
        if(filtro.getTítulo().equals("")) {
            filtro.setTítulo(null);
        }
    }
    
    private int quantidadeDisponivelIndisponivel(List<TbExemplar> exemplares,Integer isDisponivel) {
     AtomicInteger  total = new AtomicInteger(0);;
     exemplares.stream().forEach(e -> 
     { 
         if(e.getCircular() == isDisponivel) {
           total.getAndIncrement();
         }
     });
     return total.get();
    }
    
    
    /**
     * @return the acervos
     */
    public List<ConsultaAcervoDTO> getAcervos() {
        return acervos;
    }

    /**
     * @param acervos the acervos to set
     */
    public void setAcervos(List<ConsultaAcervoDTO> acervos) {
        this.acervos = acervos;
    }

    /**
     * @return the filtro
     */
    public AcervoFilter getFiltro() {
        return filtro;
    }

    /**
     * @param filtro the filtro to set
     */
    public void setFiltro(AcervoFilter filtro) {
        this.filtro = filtro;
    }

    /**
     * @return the autoresDb
     */
    public List<TbAutores> getAutoresDb() {
        return autoresDb;
    }

    /**
     * @param autoresDb the autoresDb to set
     */
    public void setAutoresDb(List<TbAutores> autoresDb) {
        this.autoresDb = autoresDb;
    }

    /**
     * @return the assuntosDb
     */
    public List<TbAssunto> getAssuntosDb() {
        return assuntosDb;
    }

    /**
     * @param assuntosDb the assuntosDb to set
     */
    public void setAssuntosDb(List<TbAssunto> assuntosDb) {
        this.assuntosDb = assuntosDb;
    }
    
}
