/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teste.lpwsd.dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author U6046370
 */
public class TipoUsuarioDTO {
    private List<String> tipos = new ArrayList<>();
    
    
    public TipoUsuarioDTO() {
        this.criarTipos();
    }

    /**
     * @return the tipos
     */
    public List<String> getTipos() {
        return tipos;
    }

    /**
     * @param tipos the tipos to set
     */
    public void setTipos(List<String> tipos) {
        this.tipos = tipos;
    }
    
    public void setTipos(String tipos) {
        this.tipos.add(tipos);
    }
    
    
    private void criarTipos() {
        this.tipos.add("Aluno");
        this.tipos.add("Professor");
        this.tipos.add("Funcionário");
        this.tipos.add("Bibliotecário");
        this.tipos.add("Administrador");
    }
    
}
