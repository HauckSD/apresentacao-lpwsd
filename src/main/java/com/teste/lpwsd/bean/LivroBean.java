/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teste.lpwsd.bean;

import com.teste.lpwsd.dao.AssuntoDao;
import com.teste.lpwsd.dao.AutorDao;
import com.teste.lpwsd.dao.EditoraDao;
import com.teste.lpwsd.dao.LivroDao;
import com.teste.lpwsd.modelo.TbAssunto;
import com.teste.lpwsd.modelo.TbAutores;
import com.teste.lpwsd.modelo.TbEditora;
import com.teste.lpwsd.modelo.TbLivro;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

/**
 *
 * @author celio
 */
@ManagedBean(name = "LivroBean")
@ViewScoped
public class LivroBean implements Serializable{
    
    
    TbLivro livro = new TbLivro();
     LivroDao livroDao;
     AutorDao autorDao;
     EditoraDao editoraDao;
     AssuntoDao assuntoDao;

            
     List livros = new ArrayList();
     List<TbAssunto> assuntosDb = new ArrayList();
     String assuntosSelecionado;
     List<TbAutores> autoresDb = new ArrayList();
     private List<TbEditora> editorasDb = new ArrayList();
     List<String> autoresSelecionados = new ArrayList();
     private String editoraSelecionada;
   
    public LivroBean(){
        livroDao =  new LivroDao();
        editoraDao = new EditoraDao();
        assuntoDao = new AssuntoDao();
        autorDao = new AutorDao();
        livros = livroDao.buscarTodos();
        assuntosDb = assuntoDao.buscarTodos();
        autoresDb = autorDao.buscarTodos();
        editorasDb = editoraDao.buscarTodos();
    }

        public void record(ActionEvent actionEvent) {
            
        livro.setTbAutoresList(autoresSelecionados());
        livro.setTbAssuntoidtbAssunto(assuntoSelecionado());
        livro.setTbEditoraidtbEditora(editoraSelecionada());
        livroDao.atualizar(livro);
        livros = livroDao.buscarTodos();
        livro = new TbLivro();
        
    }
        
    private TbAssunto assuntoSelecionado(){
        return this.assuntosDb.stream().filter(n -> n.getNomeAssunto().equals(assuntosSelecionado)).findAny().orElse(null);
    }
    
    private TbEditora editoraSelecionada(){
        return this.getEditorasDb().stream().filter(n -> n.getNomeEditora().equals(getEditoraSelecionada())).findAny().orElse(null);
    }
    
    private List<TbAutores> autoresSelecionados() {
        List<TbAutores> autores =  this.autoresDb.stream().filter(n -> autoresSelecionados.contains(n.getNomeAutor())).collect(Collectors.toList());
        return autores;
    }
        
    public void exclude(ActionEvent actionEvent) {
        livroDao.remover(livro.getIdtbLivro());
        livros = livroDao.buscarTodos();
        livro = new TbLivro();
    }

    /**
     * @return the livro
     */
    public TbLivro getLivro() {
        return livro;
    }

    /**
     * @param livro the livro to set
     */
    public void setLivro(TbLivro livro) {
        this.livro = livro;
    }

    /**
     * @return the livros
     */
    public List getLivros() {
        return livros;
    }

    /**
     * @param livros the livros to set
     */
    public void setLivros(List livros) {
        this.livros = livros;
    }

    /**
     * @return the assuntosDb
     */
    public List<TbAssunto> getAssuntosDb() {
        return assuntosDb;
    }

    /**
     * @param assuntosDb the assuntosDb to set
     */
    public void setAssuntosDb(List<TbAssunto> assuntosDb) {
        this.assuntosDb = assuntosDb;
    }


    /**
     * @return the autoresDb
     */
    public List<TbAutores> getAutoresDb() {
        return autoresDb;
    }

    /**
     * @param autoresDb the autoresDb to set
     */
    public void setAutoresDb(List<TbAutores> autoresDb) {
        this.autoresDb = autoresDb;
    }

    /**
     * @return the autoresSelecionados
     */
    public List<String> getAutoresSelecionados() {
        return autoresSelecionados;
    }

    /**
     * @param autoresSelecionados the autoresSelecionados to set
     */
    public void setAutoresSelecionados(List<String> autoresSelecionados) {
        this.autoresSelecionados = autoresSelecionados;
    }

    /**
     * @return the assuntosSelecionado
     */
    public String getAssuntosSelecionado() {
        return assuntosSelecionado;
    }

    /**
     * @param assuntosSelecionado the assuntosSelecionado to set
     */
    public void setAssuntosSelecionado(String assuntosSelecionado) {
        this.assuntosSelecionado = assuntosSelecionado;
    }

    /**
     * @return the editorasDb
     */
    public List<TbEditora> getEditorasDb() {
        return editorasDb;
    }

    /**
     * @param editorasDb the editorasDb to set
     */
    public void setEditorasDb(List<TbEditora> editorasDb) {
        this.editorasDb = editorasDb;
    }

    /**
     * @return the editoraSelecionada
     */
    public String getEditoraSelecionada() {
        return editoraSelecionada;
    }

    /**
     * @param editoraSelecionada the editoraSelecionada to set
     */
    public void setEditoraSelecionada(String editoraSelecionada) {
        this.editoraSelecionada = editoraSelecionada;
    }
    
}
