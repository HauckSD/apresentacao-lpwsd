/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teste.lpwsd.dao;
import com.teste.lpwsd.bd.PersistenceUtil;
import com.teste.lpwsd.dto.ConsultaAcervoDTO;
import com.teste.lpwsd.filter.AcervoFilter;
import com.teste.lpwsd.filter.UsuarioFilter;
import com.teste.lpwsd.modelo.TbLivro;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author celio
 */
public class LivroDao extends GenericDao<TbLivro, Integer> {
    
    public LivroDao() {
        super(TbLivro.class);
    }

    public List<TbLivro> buscarAcervo(AcervoFilter filtro) {
        try {    
        String q  = "SELECT l FROM TbLivro l "
                + "join l.tbExemplarList ex "
                + "join l.tbAssuntoidtbAssunto ass "
                + "where l.titulo like :titulo and "
                + "ass.nomeAssunto =:assunto";
        
        EntityManager manager = PersistenceUtil.getEntityManager();
        Query query = manager.createQuery(q);
        query.setParameter("titulo", "%" + filtro.getTítulo() + "%");
        query.setParameter("assunto", filtro.getAssunto());
        return query.getResultList();
        } catch (Exception e) {
           return null;
        }
    }
    
}
