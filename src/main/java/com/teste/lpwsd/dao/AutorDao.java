/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teste.lpwsd.dao;

import com.teste.lpwsd.modelo.TbAutores;

/**
 *
 * @author celio
 */
public class AutorDao extends GenericDao<TbAutores, Integer> {

    public AutorDao() {
       super(TbAutores.class);
    }
    
}
