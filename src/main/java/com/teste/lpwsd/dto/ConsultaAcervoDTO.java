/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teste.lpwsd.dto;

import java.util.List;

/**
 *
 * @author celio
 */
public class ConsultaAcervoDTO {
    private String titulo;
    private Integer quantidade;
    private Integer quantidadeDisponivel;
    private Integer quantidadeIndisponivel;
 
    
    public ConsultaAcervoDTO(String titulo, Integer quantidade, 
                            Integer quantidadeDisponivel,Integer quantidadeIndisponivel){
        this.titulo = titulo;
        this.quantidade = quantidade;
        this.quantidadeDisponivel = quantidadeDisponivel;
        this.quantidadeIndisponivel = quantidadeIndisponivel;
    }
    
     public ConsultaAcervoDTO(String titulo){
        this.titulo = titulo;
    }
    
    
    
    public ConsultaAcervoDTO() {
    }
    
    
    /**
     * @return the titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return the quantidade
     */
    public Integer getQuantidade() {
        return quantidade;
    }

    /**
     * @param quantidade the quantidade to set
     */
    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    /**
     * @return the quantidadeDisponivel
     */
    public Integer getQuantidadeDisponivel() {
        return quantidadeDisponivel;
    }

    /**
     * @param quantidadeDisponivel the quantidadeDisponivel to set
     */
    public void setQuantidadeDisponivel(Integer quantidadeDisponivel) {
        this.quantidadeDisponivel = quantidadeDisponivel;
    }

    /**
     * @return the quantidadeIndisponivel
     */
    public Integer getQuantidadeIndisponivel() {
        return quantidadeIndisponivel;
    }

    /**
     * @param quantidadeIndisponivel the quantidadeIndisponivel to set
     */
    public void setQuantidadeIndisponivel(Integer quantidadeIndisponivel) {
        this.quantidadeIndisponivel = quantidadeIndisponivel;
    }
    
    
}
