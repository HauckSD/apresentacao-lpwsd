/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teste.lpwsd.modelo;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author celio
 */
@Entity
@Table(name= "TbReserva")
public class TbReserva {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;

	@Temporal(TemporalType.DATE)
	private Date dataReserva;

	@ManyToOne
	@JoinColumn(name="idEmprestimo")
	private TbEmprestimo tbEmprestimo;

	@ManyToOne
	@JoinColumn(name="idExemplar")
	private TbExemplar tbExemplar;

	@ManyToOne
	@JoinColumn(name="idUsuario")
	private TbUsuario tbUsuario;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the dataReserva
     */
    public Date getDataReserva() {
        return dataReserva;
    }

    /**
     * @param dataReserva the dataReserva to set
     */
    public void setDataReserva(Date dataReserva) {
        this.dataReserva = dataReserva;
    }

    /**
     * @return the tbEmprestimo
     */
    public TbEmprestimo getTbEmprestimo() {
        return tbEmprestimo;
    }

    /**
     * @param tbEmprestimo the tbEmprestimo to set
     */
    public void setTbEmprestimo(TbEmprestimo tbEmprestimo) {
        this.tbEmprestimo = tbEmprestimo;
    }

    /**
     * @return the tbExemplar
     */
    public TbExemplar getTbExemplar() {
        return tbExemplar;
    }

    /**
     * @param tbExemplar the tbExemplar to set
     */
    public void setTbExemplar(TbExemplar tbExemplar) {
        this.tbExemplar = tbExemplar;
    }

    /**
     * @return the tbUsuario
     */
    public TbUsuario getTbUsuario() {
        return tbUsuario;
    }

    /**
     * @param tbUsuario the tbUsuario to set
     */
    public void setTbUsuario(TbUsuario tbUsuario) {
        this.tbUsuario = tbUsuario;
    }
}
