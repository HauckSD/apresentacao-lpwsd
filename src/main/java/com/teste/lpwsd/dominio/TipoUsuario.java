/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teste.lpwsd.dominio;

/**
 *
 * @author chjunior1
 *         this.tipos.add("Aluno");
        this.tipos.add("Professor");
        this.tipos.add("Funcionário");
        this.tipos.add("Bibliotecário");
        this.tipos.add("Administrador");
 */
public enum TipoUsuario {
    ALUNO("Aluno"),
    PROFESSOR("Professor"),
    FUNCIONARIO("Funcionário"),
    BIBLIOTECARIO("Bibliotecário"),
    ADMINISTRADOR("Administrador");
    
    
    private String tipo;
 
    TipoUsuario(String tipo) {
        this.tipo = tipo;
    }
 
    public String getTipo() {
        return tipo;
    }
}
