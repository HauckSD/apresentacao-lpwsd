/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teste.lpwsd.dao;
import com.teste.lpwsd.dto.EmprestimoDTO;
import com.teste.lpwsd.dto.ExemplarDTO;
import com.teste.lpwsd.dto.LivroDTO;
import com.teste.lpwsd.dto.ReservaDTO;
import com.teste.lpwsd.dto.UsuarioDTO;
import com.teste.lpwsd.modelo.TbExemplar;
import com.teste.lpwsd.modelo.TbReserva;
import com.teste.lpwsd.modelo.TbUsuario;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author celio
 */
public class ReservaDao extends GenericDao<TbReserva, Integer>{
    
        private ExemplarDao exemplarDao;
        private UsuarioDao usuarioDao;
                
        public ReservaDao(){
        super(TbReserva.class);
        this.exemplarDao = new ExemplarDao();
        this.usuarioDao = new UsuarioDao();
        }
        
        @SuppressWarnings("unchecked")
	public List<ReservaDTO> obterReservas() {
		StringBuilder hql = new StringBuilder();
		List<ReservaDTO> reservas = new ArrayList<ReservaDTO>();
		hql.append("SELECT DISTINCT \n" +
                                " res.id AS id_reserva, \n" +
                                " \n " +
                                " res.dataReserva AS data_reserva,\n" +
                                " \n " +
                                " exe.idtbExemplar as id_exemplar,\n" +
                                " \n " +
                                " liv.idtbLivro AS id_livro, \n " +
                                " \n " +
                                " liv.titulo AS titulo, \n " +
                                " \n " +
                                " liv.isbn AS ISBN, \n " +
                                " \n " +
                                " liv.ano AS ANO, \n " +
                                " \n " +
                                " usu.idtbUsuario AS id_usuario, \n " +
                                " \n" +
                                " usu.nomeUsuario AS nm_usuario, \n " +
                                " \n " +
                                " usu.email AS email_usuario, \n" +
                                " \n " +
                                " emp.idtbEmprestimo AS id_emprestimo, \n " +
                                " emp.dataEmprestimo AS data_emprestimo, \n " +
                                " emp.dataDevolucao AS data_devolucao \n " +
                                " \n " +
                                " FROM tbreserva AS res \n " +
                                " \n " +
                                " inner join tbexemplar as exe on res.idExemplar = exe.idtbExemplar \n " +
                                " \n " +
                                " inner join tbusuario as usu on res.idUsuario = usu.idtbUsuario \n " +
                                " \n " +
                                " inner join tblivro as liv on liv.idtbLivro = exe.tbLivro_idtbLivro \n" +
                                " left join tbemprestimo as emp on res.idEmprestimo = emp.idtbEmprestimo ");
		
		List<Object[]> results =  this.getManager().createNativeQuery(hql.toString()).getResultList();
		 
		results.stream().forEach((response) -> { 
			ReservaDTO reservaModel = new ReservaDTO();
			UsuarioDTO pessoaModel = new UsuarioDTO();
			LivroDTO livroModel = new LivroDTO();
			EmprestimoDTO emprestimoModel = new EmprestimoDTO();
			ExemplarDTO exemplarModel = new ExemplarDTO();
			
			reservaModel.setIdReserva((Integer) response[0]);
			reservaModel.setDataReserva((Date) response[1]);
				livroModel.setIdLivro((Integer) response[3]);
				livroModel.setTitulo((String) response[4]);
				livroModel.setIsbn((String) response[5]);
				livroModel.setAno(recuperarAno((Date) response[6]));
					exemplarModel.setIdExemplar((Integer) response[2]);
					exemplarModel.setLivroDTO(livroModel);
			reservaModel.setExemplarDTO(exemplarModel);
				pessoaModel.setIdUsuario(Long.parseLong(((Integer) response[7]).toString()));
				pessoaModel.setUsuario((String) response[8]); 
				pessoaModel.setEmail((String) response[9]);
			reservaModel.setUsuarioDTO(pessoaModel);
				if(response[10] != null) {
					emprestimoModel.setIdEmprestimo((Integer) response[10]);
					emprestimoModel.setDataEmprestimo((Date) response[11]);
					emprestimoModel.setDataEmprestimo((Date) response[12]);
			reservaModel.setEmprestimoDTO(emprestimoModel); 
				}
			reservas.add(reservaModel);
			
		});
		return reservas;
	}
        
        
        private int recuperarAno(Date date) {
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(date);
        return calendar1.get(Calendar.YEAR);
        }
        
            public void salvarReservar(LivroDTO livroModel, Long idUsuario, Date dataRetirada) {
            TbReserva reservaEntity = new TbReserva();
            reservaEntity.setDataReserva(dataRetirada);
                    TbExemplar exemplarEntity = this.exemplarDao.buscarPorId(livroModel.getIdExemplar());
            reservaEntity.setTbExemplar(exemplarEntity);
                    TbUsuario usuarioEntity = this.usuarioDao.buscarPorId(idUsuario.intValue());
            reservaEntity.setTbUsuario(usuarioEntity);
           this.salvar(reservaEntity);
	} 
}
