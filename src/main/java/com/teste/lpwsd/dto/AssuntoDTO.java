/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teste.lpwsd.dto;

/**
 *
 * @author celio
 */
public class AssuntoDTO {
    	private Integer idAssunto;
	private String assunto;

    /**
     * @return the idAssunto
     */
    public Integer getIdAssunto() {
        return idAssunto;
    }

    /**
     * @param idAssunto the idAssunto to set
     */
    public void setIdAssunto(Integer idAssunto) {
        this.idAssunto = idAssunto;
    }

    /**
     * @return the assunto
     */
    public String getAssunto() {
        return assunto;
    }

    /**
     * @param assunto the assunto to set
     */
    public void setAssunto(String assunto) {
        this.assunto = assunto;
    }
}
