package com.teste.lpwsd.bean;

import com.teste.lpwsd.dao.UsuarioDao;
import com.teste.lpwsd.dto.TipoUsuarioDTO;
import com.teste.lpwsd.modelo.TbUsuario;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author U6046370
 */
@ManagedBean(name = "CadastroUsuarioBean")
@ViewScoped()
public class CadastroUsuarioBean implements Serializable {
    
    private TbUsuario usuario = new TbUsuario();
    UsuarioDao usuarioDao;
    private List<String> tipos = new ArrayList();
    TipoUsuarioDTO tipo;
    private List usuarios = new ArrayList();
    
    public CadastroUsuarioBean(){
        tipo = new TipoUsuarioDTO();
        tipos = tipo.getTipos();
        usuarioDao =  new UsuarioDao();
        usuarios = usuarioDao.buscarTodos();
    }
    
    //Métodos dos botões 
    public void record(ActionEvent actionEvent) {
        usuarioDao.atualizar(getUsuario());
        setUsuarios(usuarioDao.buscarTodos());
        setAutor(new TbUsuario());
    }

    public void exclude(ActionEvent actionEvent) {
        usuarioDao.remover(getUsuario().getIdtbUsuario());
        setUsuarios(usuarioDao.buscarTodos());
        setAutor(new TbUsuario());
    }

    public TbUsuario getUsuario() {
        return usuario;
    }

    public void setAutor(TbUsuario usuario) {
        this.usuario = usuario;
    }
    
    public List getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List usuarios) {
        this.usuarios = usuarios;
    }

    /**
     * @return the tipos
     */
    public List<String> getTipos() {
        return tipos;
    }

    /**
     * @param tipos the tipos to set
     */
    public void setTipos(List<String> tipos) {
        this.tipos = tipos;
    }
}
