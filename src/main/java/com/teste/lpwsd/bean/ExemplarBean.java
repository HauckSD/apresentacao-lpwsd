/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teste.lpwsd.bean;

import com.teste.lpwsd.dao.EditoraDao;import com.teste.lpwsd.dao.ExemplarDao;
;
import com.teste.lpwsd.modelo.TbEditora;
import com.teste.lpwsd.modelo.TbExemplar;
import com.teste.lpwsd.dao.LivroDao;
import com.teste.lpwsd.modelo.TbAutores;
import com.teste.lpwsd.modelo.TbLivro;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

/**
 *
 * @author celio
 */
@ManagedBean(name = "ExemplarBean")
@ViewScoped()
public class ExemplarBean implements Serializable {
    
    private TbExemplar exemplar = new TbExemplar();
    ExemplarDao exemplarDao;
    private List exemplares = new ArrayList();
    
    String livrosSelecionado;
    LivroDao livroDao;
    List<TbLivro> livrosDb = new ArrayList();
    
    public ExemplarBean(){
        livroDao = new LivroDao();
        exemplarDao =  new ExemplarDao();
        exemplares = exemplarDao.buscarTodos();
        livrosDb = livroDao.buscarTodos();
    }
    
    //Métodos dos botões 
    public void record(ActionEvent actionEvent) {
        exemplar.setTbLivroList(livroSelecionado());
        exemplarDao.atualizar(exemplar);
        setExemplares(exemplarDao.buscarTodos());
        exemplar = new TbExemplar();
        
    }

    public void exclude(ActionEvent actionEvent) {
        exemplarDao.remover(getExemplar().getIdtbExemplar());
        setExemplares(exemplarDao.buscarTodos());
        setExemplar(new TbExemplar());
    }
    
    private TbLivro livroSelecionado(){
        return this.livrosDb.stream().filter(n -> n.getTitulo().equals(livrosSelecionado)).findAny().orElse(null);
    }
    
    public TbExemplar getExemplar() {
        return exemplar;
    }

    public void setExemplar(TbExemplar exemplar) {
        this.exemplar = exemplar;
    }
    
    public String getLivrosSelecionado() {
        return livrosSelecionado;
    }

    public void setLivrosSelecionado(String livrosSelecionado) {
        this.livrosSelecionado = livrosSelecionado;
    }

    public List<TbLivro> getLivrosDb() {
        return livrosDb;
    }

    public void setLivrosDb(List<TbLivro> livrosDb) {
        this.livrosDb = livrosDb;
    }

    /**
     * @return the exemplares
     */
    public List getExemplares() {
        return exemplares;
    }

    /**
     * @param exemplares the exemplares to set
     */
    public void setExemplares(List exemplares) {
        this.exemplares = exemplares;
    }

}
