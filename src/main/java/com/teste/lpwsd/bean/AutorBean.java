/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teste.lpwsd.bean;

import com.teste.lpwsd.dao.AutorDao;
import com.teste.lpwsd.modelo.TbAutores;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

/**
 *
 * @author celio
 */
@ManagedBean(name = "AutorBean")
@ViewScoped
public class AutorBean implements Serializable {
    
    
    private TbAutores autor = new TbAutores();
    AutorDao autorDao;
    private List autores = new ArrayList();
    
    public AutorBean(){
        autorDao =  new AutorDao();
        autores = autorDao.buscarTodos();
    }
    
        //Métodos dos botões 
    public void record(ActionEvent actionEvent) {
        autorDao.atualizar(getAutor());
        setAutores(autorDao.buscarTodos());
        setAutor(new TbAutores());
    }

    public void exclude(ActionEvent actionEvent) {
        autorDao.remover(getAutor().getIdtbAutores());
        setAutores(autorDao.buscarTodos());
        setAutor(new TbAutores());
    }

    /**
     * @return the autor
     */
    public TbAutores getAutor() {
        return autor;
    }

    /**
     * @param autor the autor to set
     */
    public void setAutor(TbAutores autor) {
        this.autor = autor;
    }

    /**
     * @return the autores
     */
    public List getAutores() {
        return autores;
    }

    /**
     * @param autores the autores to set
     */
    public void setAutores(List autores) {
        this.autores = autores;
    }

}
