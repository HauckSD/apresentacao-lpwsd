/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teste.lpwsd.dto;

/**
 *
 * @author celio
 */
public class EditoraDTO {
    	private Integer idEditora;
	private String nome;

    /**
     * @return the idEditora
     */
    public Integer getIdEditora() {
        return idEditora;
    }

    /**
     * @param idEditora the idEditora to set
     */
    public void setIdEditora(Integer idEditora) {
        this.idEditora = idEditora;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }
}
