/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teste.lpwsd.bd;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author celio
 */
public class PersistenceUtil {
    	private static EntityManagerFactory emf;
	
	private static EntityManagerFactory getEntityManagerFactory(){
		if(emf==null){
			emf = Persistence.createEntityManagerFactory("LPWSD");
		}
		return emf;
	}
	
	public static EntityManager getEntityManager(){
		return getEntityManagerFactory().createEntityManager();
	}
}
