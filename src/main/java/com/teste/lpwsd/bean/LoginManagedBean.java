package com.teste.lpwsd.bean;

import com.teste.lpwsd.dao.UsuarioDao;
import com.teste.lpwsd.util.SessionUtil;
import com.teste.lpwsd.modelo.TbUsuario;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
  
@ManagedBean(name = "LoginManagedBean")
@ViewScoped
public class LoginManagedBean implements Serializable {
  
      UsuarioDao usuarioDAO = new UsuarioDao();
      TbUsuario usuario = new TbUsuario();
      SessionUtil sessao;
       
      public String envia() {
             
            usuario = usuarioDAO.getUsuario(usuario.getNomeUsuario(), usuario.getSenha());
            if (usuario == null) {
                  usuario = new TbUsuario();
                  FacesContext.getCurrentInstance().addMessage(
                             null,
                             new FacesMessage(FacesMessage.SEVERITY_ERROR, "Usuário não encontrado!",
                                         "Erro no Login!"));
                  return null;
            } else {
                sessao.setParam("USUARIOLogado",usuario);
                  return "/index";
            }        
      }
  
      public TbUsuario getUsuario() {
            return usuario;
      }
  
      public void setUsuario(TbUsuario usuario) {
            this.usuario = usuario;
      }

}